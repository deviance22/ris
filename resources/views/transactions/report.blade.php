@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="form-group row pb-3">
        <div class="col-md-2 d-print-none">
            <form action="" method="get" class="form-inline">
                <input type="date" name="date" id="date" class="form-control mr-3" value="{{ $date->isoFormat('YYYY-MM-DD') }}">
                <button class="btn btn-primary btn-sm" type="submit">Submit</button>
            </form>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-md-12">
            <h3 class="text-center"> REPORT OF SUPPLIES AND MATERIALS ISSUED</h3>
        </div>
    </div>
    <div class="row p-3">
        <div class="col-md-6">
            <p>Entity Name: ________________________</p>
        </div>
        <div class="col-md-6">
            <p class="text-right">Serial No.: ________________________</p>
        </div>
    </div>
    <div class="row p-3">
        <div class="col-md-6">
            <p>Fund Cluster: ________________________</p>
        </div>
        <div class="col-md-6">
            <p class="text-right">Date: {{ $date->isoFormat('YYYY-MM-DD') }}</p>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table-bordered table-sm">
                <tbody>
                    <th colspan="6" class="text-center">To be filled up by the Supply and/or Property Division/Unit</th>
                    <th colspan="2" class="text-center">To be filled up by the Accounting Division/Unit</th>
                </tbody>
                <tbody class="text-center">
                    <th>RIS No.</th>
                    <th>Responsibility Center Code</th>
                    <th>Stock No.</th>
                    <th>Item</th>
                    <th>Unit</th>
                    <th>Quantity Issued</th>
                    <th>Unit Cost</th>
                    <th>Amount</th>
                </tbody>
                <tbody class="text-center">
                    @foreach($transactions as $transaction)
                    <tr>
                        <td>{{ $transaction->ris_no }}</td>
                        <td></td>
                        <td>{{ $transaction->stock_no }}</td>
                        <td>{{ $transaction->description }}</td>
                        <td>{{ $transaction->unit }}</td>
                        <td>{{ $transaction->quantity_issued }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endforeach
                </tbody>
                <tbody class="text-center">
                    <th></th>
                    <th colspan="2">Recapitulation</th>
                    <th></th>
                    <th></th>
                    <th colspan="3">Recapitulation</th>
                </tbody>
                <tbody class="text-center">
                    <th></th>
                    <th>Stock No.</th>
                    <th>Quantity</th>
                    <th></th>
                    <th></th>
                    <th>Unit Cost</th>
                    <th>Total Cost</th>
                    <th>UAC's Object Code</th>
                </tbody>
                <tbody>
                    @foreach($transactions as $transaction)
                    <tr>
                        <td></td>
                        <td>{{ $transaction->stock_no }}</td>
                        <td>{{ $transaction->quantity_issued }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="5"></td>
                        <td colspan="3">Posted by:</td>
                    </tr>
                    <tr>
                        <td colspan="5">I hereby certify to the correctness of the above information.</td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3" class="text-center pt-5">_______________________________________________________________</td>
                        <td></td>
                        <td colspan="2" class="text-center pt-5">____________________________</td>
                        <td class="text-center pt-5">______________</td>
                    </tr>
                    <tr>
                    <td></td>
                        <td colspan="3" class="text-center">Signature over Printed Name of Supply and/or Property Custodian</td>
                        <td></td>
                        <td colspan="2" class="text-center">Signature over Printed Name of Designated Accounting Staff</td>
                        <td class="text-center">Date</td>
                    </tr>

                </tbody>
            </table>
            
        </div>
    </div>
</div>
@endsection
