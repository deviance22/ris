@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="form-group row pb-3">
        <div class="col-md-2 d-print-none">
            <form action="" method="get" class="form-inline">
                <input type="date" name="date" id="date" class="form-control mr-3" value="{{ $date->isoFormat('YYYY-MM-DD') }}">
                <button class="btn btn-primary btn-sm" type="submit">Submit</button>
            </form>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-md-12">
            <h3 class="text-center">REQUISITION AND ISSUE SLIP</h3>
        </div>
    </div>
    <div class="row p-3">
        <div class="col-md-6">
            <p>Entity Name: ________________________</p>
        </div>
        <div class="col-md-6">
            <p class="text-right">Fund Cluster : ___________01_________________</p>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr>
                        <td colspan="5">Division:</td>
                        <td colspan="4">Responsibility Center Code:</td>
                    </tr>
                    <tr>
                        <td colspan="5">Office:</td>
                        <td colspan="4">RIS No:</td>
                    </tr>
                </tbody>
                <tbody>
                    <th colspan="5" class="text-center">Requisition</th>
                    <th colspan="2" class="text-center">Stock Available?</th>
                    <th colspan="2" class="text-center">Issue</th>
                </tbody>
                <tbody class="text-center">
                    <th>Stock No.</th>
                    <th>Unit</th>
                    <th colspan="2">Description</th>
                    <th>Quantity</th>
                    <th>Yes</th>
                    <th>No</th>
                    <th>Quantity</th>
                    <th style="column-width: 200px">Remarks</th>
                </tbody>
                <tbody class="text-center">
                    @foreach($transactions as $transaction)
                    <tr>
                        <td>{{ $transaction->stock_no }}</td>
                        <td>{{ $transaction->unit }}</td>
                        <td colspan="2">{{ $transaction->description }}</td>
                        <td>{{ $transaction->quantity_requested }}</td>
                        <td>{{ $transaction->approved ? "√" : "" }}</td>
                        <td>{{ $transaction->approved == 2 ? "√" : "" }}</td>
                        <td>{{ $transaction->quantity_issued }}</td>
                        <td></td>
                    </tr>
                    @endforeach
                </tbody>
                <tbody>
                    <th colspan="9">Purpose:</th>
                </tbody>
                <tbody class="text-center">
                    <th colspan="2"></th>
                    <th>Requested by:</th>
                    <th>Approved by:</th>
                    <th colspan="3">Issued by:</th>
                    <th colspan="2">Received by:</th>
                </tbody>
                <tbody class="text-right">
                    <th colspan="2">Signature:</th>
                    <th></th>
                    <th></th>
                    <th colspan="3"></th>
                    <th colspan="2"></th>
                </tbody>
                <tbody class="text-right">
                    <th colspan="2">Printed Name:</th>
                    <th></th>
                    <th></th>
                    <th colspan="3"></th>
                    <th colspan="2"></th>
                </tbody>
                <tbody class="text-right">
                    <th colspan="2">Designation:</th>
                    <th></th>
                    <th></th>
                    <th colspan="3"></th>
                    <th colspan="2"></th>
                </tbody>
                <tbody class="text-right">
                    <th colspan="2">Date:</th>
                    <th></th>
                    <th></th>
                    <th colspan="3"></th>
                    <th colspan="2"></th>
                </tbody>
            </table>
            
        </div>
    </div>
</div>
@endsection
