@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table">
                <thead class="text-center">
                    <th colspan="4">Requisition</th>
                    <th colspan="2">Stock Available</th>
                    <th colspan="2">Issue</th>
                </thead>
                <thead>
                    <th>Stock No.</th>
                    <th>Unit</th>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Yes</th>
                    <th>No</th>
                    <th>Quantity</th>
                    <th>Remark</th>
                </thead>
                <tbody>
                    @foreach($transactions as $transaction)
                    <tr>
                        <td>{{ $transaction->ris_no }}</td>
                        <td>{{ $transaction->stock_no }}</td>
                        <td>{{ $transaction->unit }}</td>
                        <td>{{ $transaction->description }}</td>
                        <td>{{ $transaction->quantity_issued }}</td>
                        <td>{{ $transaction->quantity_requested }}</td>
                        <td>{{ $transaction->first_name }}  {{ $transaction->last_name }}</td>
                        <td>{{ $status[$transaction->approved]  }}</td>
                        @if($transaction->approved == 0)
                        <td><a href="{{ route('transactions.edit', $transaction->id) }}"><button class="btn btn-success">Approve</button></a></td>
                        <td><form action="{{ route('transactions.destroy', $transaction->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger">Reject</button>
                            </form></td>
                        @else
                        <td></td>
                        <td></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
