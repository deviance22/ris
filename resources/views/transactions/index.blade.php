@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row pb-3">
        <div class="col-md-12">
            @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                {{ Session::get('error') }}
            </div>
            @endif
            @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{ Session::get('success') }}
            </div>
            @endif
        </div>
    </div>
    <div class="row justify-content-center pb-3">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Request Items') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('transactions.request') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="id" class="col-md-4 col-form-label text-md-right">{{ __('Stock No') }}</label>

                            <div class="col-md-6">
                                <select name="id" id="id" class="form-control selectpicker" data-live-search="true">
                                    @foreach($items as $item)
                                    <option value="{{ $item->id }}">{{ $item->description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="quantity_requested" class="col-md-4 col-form-label text-md-right">{{ __('Requested Quantity') }}</label>

                            <div class="col-md-6">
                                <input id="quantity_requested" type="number" class="form-control" name="quantity_requested" required autocomplete="quantity">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="added_on" class="col-md-4 col-form-label text-md-right">{{ __('RIS Date') }}</label>

                            <div class="col-md-6">
                                <input id="added_on" type="date" class="form-control" name="added_on" value="{{ $date->isoFormat('YYYY-MM-DD') }}" required>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Add') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-md-12">
            <a href="{{ url('ris') }}" class="btn btn-primary">RIS</a>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table">
                <thead>
                    <th>Stock No</th>
                    <th>Description</th>
                    <th>Quantity Issued</th>
                    <th>Quantity Requested</th>
                    <th>Approval Status</th>
                    <th></th>
                </thead>
                <tbody>
                    @foreach($transactions as $transaction)
                    <tr>
                        <td>{{ $transaction->stock_no }}</td>
                        <td>{{ $transaction->description }}</td>
                        <td>{{ $transaction->quantity_issued }}</td>
                        <td>{{ $transaction->quantity_requested }}</td>
                        <td>{{ $status[$transaction->approved]  }}</td>
                        @if($transaction->approved == 0)
                        <td><form action="{{ route('transactions.destroy', $transaction->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger">Delete</button>
                            </form></td>
                        @else
                        <td></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{ $transactions->links() }}
</div>
@endsection