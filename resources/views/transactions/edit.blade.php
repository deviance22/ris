@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Approve Transaction') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('transactions.update', $transaction->id) }}">
                        @csrf
                        @method('PATCH')

                        <div class="form-group row">
                            <label for="stock_no" class="col-md-4 col-form-label text-md-right">{{ __('Stock No') }}:</label>
                            <label for="stock_no" class="col-md-4 col-form-label text-md-left">{{ $transaction->stock_no }}</label>
                        </div>

                        <div class="form-group row">
                            <label for="unit" class="col-md-4 col-form-label text-md-right">{{ __('Unit') }}: </label>
                            <label for="stock_no" class="col-md-4 col-form-label text-md-left">{{ $transaction->unit }}</label>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}: </label>
                            <label for="stock_no" class="col-md-4 col-form-label text-md-left">{{ $transaction->description }}</label>
                        </div>

                        <div class="form-group row">
                            <label for="quantity_requested" class="col-md-4 col-form-label text-md-right">{{ __('Quantity Requested') }}: </label>
                            <label for="stock_no" class="col-md-4 col-form-label text-md-left">{{ $transaction->quantity_requested }}</label>
                        </div>
                        <div class="form-group row">
                            <label for="ris_no" class="col-md-4 col-form-label text-md-right">{{ __('RIS No.') }}</label>

                            <div class="col-md-6">
                                <input id="ris_no" type="text" class="form-control" name="ris_no" required autocomplete="ris_no">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="quantity_issued" class="col-md-4 col-form-label text-md-right">{{ __('Quantity to issue') }}</label>

                            <div class="col-md-6">
                                <input id="quantity_issued" type="number" class="form-control" name="quantity_issued" required autocomplete="quantity_issued">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Approve') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
