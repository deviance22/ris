@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row pb-3">
        <div class="col-md-12">
            @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{ Session::get('success') }}
            </div>
            @endif
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-md-12">
            <a href="{{ url('report') }}" class="btn btn-primary">RSMI</a>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table">
                <thead>
                    <th>RIS No.</th>
                    <th>Stock No.</th>
                    <th>Unit</th>
                    <th>Item</th>
                    <th>Quantity Issued</th>
                    <th>Quantity Requested</th>
                    <th>Quantity Replenished</th>
                    <th>Requested By</th>
                    <th>Approval Status</th>
                    <th colspan="2"></th>
                </thead>
                <tbody>
                    @foreach($transactions as $transaction)
                    <tr>
                        <td>{{ $transaction->ris_no ? $transaction->ris_no : $transaction->iar_no }}</td>
                        <td>{{ $transaction->stock_no }}</td>
                        <td>{{ $transaction->unit }}</td>
                        <td>{{ $transaction->description }}</td>
                        <td>{{ $transaction->ris_no ? $transaction->quantity_issued : "" }}</td>
                        <td>{{ $transaction->quantity_requested}}</td>
                        <td>{{ $transaction->iar_no ? $transaction->quantity_issued : ""}}</td>
                        <td>{{ $transaction->first_name }}  {{ $transaction->last_name }}</td>
                        <td>{{ $status[$transaction->approved]  }}</td>
                        @if($transaction->approved == 0)
                        <td><a href="{{ route('transactions.edit', $transaction->id) }}"><button class="btn btn-success">Approve</button></a></td>
                        <td><form action="{{ route('transactions.destroy', $transaction->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger">Reject</button>
                            </form></td>
                        @else
                        <td></td>
                        <td></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{ $transactions->links() }}
</div>
@endsection
