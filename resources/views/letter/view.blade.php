@extends('layouts.blank')

@section('content')
<script>
    const password = "{{ $letter->password }}";
    let promptPassword = prompt('Enter password to view page', '');
    while (password != promptPassword) {
        console.log('password', password);
        console.log('promptPassword', promptPassword);
        promptPassword = prompt('Enter password to view page', '');
    }
</script>
<style>

    body {
        background-image: url("../img/bg.jpg");
        background-repeat: no-repeat;
        background-size: cover;
        height: 100%;
    }

    #letter-container {
        background-image: url("../img/paper.jpg");
        background-repeat: no-repeat, repeat;
        background-position: center top;
        height: 1015px;
        font-family: 'Alex Brush', 'Arial', serif;
        width: 769px;
    }

    #letter-container > div > p {
        font-size: 20px;
        word-wrap: break-word;
    }

</style>
<div class="container">
    <div class="row">
        <div id="letter-container" class="col-md-12">
            <div class="col-md-8 offset-md-2 mt-5 text-wrap text-justify">
                {!! $letter->content !!}
            </div>
        </div>
    </div>
</div>

@endsection