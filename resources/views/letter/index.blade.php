@extends('layouts.blank')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Add message') }}</div>

                <div class="card-body">
                    @if($letter)
                    <form method="POST" action="{{ route('letters.update', $letter->id) }}">
                        @csrf
                        @method('PATCH')
                        <div class="form-group row">
                            <label for="content" class="col-md-2 col-form-label">{{ __('Content') }}</label>
                            <div class="col-md-10">
                                <textarea name="content" id="editor" class="form-control">
                                    {{ $letter->content }}
                                </textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label">{{ __('Password') }}</label>

                            <div class="col-md-10">
                                <input id="password" type="text" class="form-control" name="password" required autocomplete="password" value="{{ $letter->password }}">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-5">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                                <a href="{{ route('letter.view') }}" class="btn btn-warning" target="_blank">
                                    View
                                </a>
                            </div>
                        </div>
                    </form>
                    @else
                    <form method="POST" action="{{ route('letters.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="content" class="col-md-2 col-form-label">{{ __('Content') }}</label>
                            <div class="col-md-10">
                                <textarea name="content" id="editor" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label">{{ __('Password') }}</label>

                            <div class="col-md-10">
                                <input id="password" type="text" class="form-control" name="password" required autocomplete="password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-6">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    ClassicEditor.create( document.querySelector( '#editor' ), {
        toolbar: [ 'Heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote' ]
    } );
</script>
@endsection
