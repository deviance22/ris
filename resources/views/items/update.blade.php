@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add Stock for ') }}{{ $item->description }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('items.updateStock', $id) }}">
                        @csrf

                        <div class="form-group row">
                            <label for="iar_no" class="col-md-4 col-form-label text-md-right">{{ __('IAR No.') }}</label>

                            <div class="col-md-6">
                                <input id="iar_no" type="text" class="form-control @error('iar_no') is-invalid @enderror" name="iar_no" required autocomplete="unit" autofocus>

                                @error('iar_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="added_on" class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>

                            <div class="col-md-6">
                                <input type="date" name="added_on" id="added_on" class="form-control mr-3" value="{{ $date->isoFormat('YYYY-MM-DD') }}">

                                @error('added_on')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="quantity" class="col-md-4 col-form-label text-md-right">{{ __('Quantity') }}</label>

                            <div class="col-md-6">
                                <input id="quantity" type="number" class="form-control" name="quantity" required autocomplete="quantity">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Add') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
