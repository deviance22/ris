@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <!-- <div class="form-group row pb-3">
        <div class="col-md-2 d-print-none">
            <form action="" method="get" class="form-inline">
                <input type="date" name="date" id="date" class="form-control mr-3" value="{{ $date->isoFormat('YYYY-MM-DD') }}">
                <button class="btn btn-primary btn-sm" type="submit">Submit</button>
            </form>
        </div>
    </div> -->
    <div class="row pb-3">
        <div class="col-md-12">
            <p class="text-right">Appendix 58</p>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-md-12">
            <h2 class="text-center">STOCK CARD</h2>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-md-9">
            <p>Entity Name:</p>
        </div>
        <div class="col-md-3">
            <p>Fund Cluster:</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr>
                        <td colspan="5">Item: {{ $item->description }}</td>
                        <td colspan="2">Stock No: {{ $item->stock_no }}</td>
                    </tr>
                    <tr>
                        <td colspan="5">Description: </td>
                        <td colspan="2">Re-order Point: </td>
                    </tr>
                    <tr>
                        <td colspan="5">Unit of Measurement: {{ $item->unit }}</td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td rowspan="2">Date</td>
                        <td rowspan="2">Reference</td>
                        <td>Receipt</td>
                        <td colspan="2">Issue</td>
                        <td>Balance</td>
                        <td rowspan="2">No. of Days to Consume</td>
                    </tr>
                    <tr>
                        <td>Qty.</td>
                        <td>Qty</td>
                        <td>Office</td>
                        <td>Qty.</td>
                    </tr>
                    @php
                        $total = 0;
                    @endphp
                    @foreach($records as $record)
                        @php
                        if($record->iar_no) {
                            $new_total = $total + $record->quantity_issued;
                        } else {
                            $new_total = $total - $record->quantity_issued;
                        }
                        
                        $total = $new_total;
                        @endphp
                    <tr>
                        <td>{{ $record->added_on }}</td>
                        <td>{{ $record->ris_no ? $record->ris_no : $record->iar_no }}</td>
                        <td>{{ $record->iar_no ? $record->quantity_issued : '' }}</td>
                        <td>{{ $record->ris_no ? $record->quantity_issued : '' }}</td>
                        <td></td>
                        <td>{{ $new_total }}</td>
                        <td></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="5">Current Balance</td>
                        <td>{{ $item->quantity }}</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
@endsection
