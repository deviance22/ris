@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 pb-3">
            <a href="{{ route('items.create') }}"><button class="btn btn-primary">Add New Item</button></a>
        </div>
    </div>
    <form method="GET" action="{{ route('items.index') }}">
        <div class="row justify-content-end form-group">
            <div class="col-6">
                <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ $description }}" autocomplete="description">
            </div>
            <div class="col-1">
                <button class="btn btn-dark">Search</button>
            </div>
        </div>
    </form>
    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session::get('success') }}
    </div>
    @endif
    @if(Session::has('delete'))
    <div class="alert alert-danger" role="alert">
        {{ Session::get('delete') }}
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <table class="table table">
                <thead>
                    <th>Stock No</th>
                    <th>Unit</th>
                    <th>Description</th>
                    <th>Quantity</th>
                    <th colspan="4">Action</th>
                </thead>
                <tbody>
                    @if(count($items) == 0)
                        <tr>
                            <td colspan="8">No items found...</td>
                        </tr>
                    @else
                        @foreach($items as $item)
                        <tr>
                            <td>{{ $item->stock_no }}</td>
                            <td>{{ $item->unit }}</td>
                            <td>{{ $item->description }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>
                                <a href="{{ route('items.add', $item->id) }}"><button class="btn btn-primary">Add</button></a>
                            </td>
                            <td>
                                <a href="{{ route('items.edit', $item->id) }}"><button class="btn btn-success">Edit</button></a>
                            </td>
                            <td>
                                <form action="{{ route('items.destroy', $item->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                            <td><a href="{{ route('items.show', $item->id) }}"><button class="btn btn-warning">Stock</button></a></td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        {{ $items->appends(request()->input())->links() }}
    </div>
    
</div>
@endsection
