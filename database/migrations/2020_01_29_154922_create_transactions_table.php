<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description', 256);
            $table->integer('quantity_issued');
            $table->integer('quantity_requested');
            $table->string('unit', 128);
            $table->string('stock_no', 64);
            $table->boolean('approved')->default(false);
            $table->integer('item_id')->default(0);
            $table->string('ris_no')->default("");
            $table->string('iar_no')->default("");
            $table->integer('user_id', false)->unsigned();
            $table->date('added_on');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
