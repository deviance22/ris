<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    return [
        'stock_no'    => $faker->sentence(2),
        'unit'        => $faker->sentence(1),
        'description' => $faker->sentence(15),
        'quantity'    => $faker->randomDigitNotNull,
    ];
});
