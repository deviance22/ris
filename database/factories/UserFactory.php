<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'username' => $faker->word,
        'first_name' => $faker->firstNameFemale,
        'last_name' => $faker->lastName,
        'password' => 'test',
        'isAdmin' => 0
    ];
});
