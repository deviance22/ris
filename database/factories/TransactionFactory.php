<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Transaction;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {
    return [
        'description'        => $faker->sentence(15),
        'unit'               => $faker->sentence(1),
        'quantity_issued'    => $faker->randomDigitNotNull,
        'quantity_requested' => $faker->randomDigitNotNull,
        'stock_no'           => $faker->sentence(2),
        'ris_no'             => $faker->date($format = 'Y-m-d', $max = 'now'),
        'iar_no'             => $faker->date($format = 'Y-m-d', $max = 'now'),
        'approved'           => $faker->boolean($chanceOfGettingTrue = 90),
        'user_id'            => 1,
        'item_id'            => 1,
    ];
});
