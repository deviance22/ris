<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'stock_no',
        'unit',
        'description',
        'quantity_requested',
        'quantity_issued',
        'approved',
        'item_id',
        'ris_no',
        'iar_no',
        'user_id',
        'added_on'
    ];
}
