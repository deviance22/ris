<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'stock_no',
        'unit',
        'description',
        'quantity'
    ];
}
