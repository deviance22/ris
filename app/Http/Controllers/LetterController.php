<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Letter;
use Redirect;


class LetterController extends Controller
{
    
    /**
     * Display the Letter
     */

    public function index()
    {

    }

    public function create()
    {
        $data['letter'] = Letter::select('*')->first();
        return view('letter/index', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'content'  => 'required',
            'password' => 'required'
        ]);
        
        Letter::create($request->all());
        return Redirect::to('letter/create');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'content'  => 'required',
            'password' => 'required'
        ]);

        $update = [
            'content'  => $request->content,
            'password' => $request->password
        ];
        
        Letter::where('id', $id)->update($update);
        return Redirect::to('letter/create');
    }

    public function view()
    {
        $data['letter'] = Letter::select('*')->first();
        return view('letter/view', $data);
    }
}
