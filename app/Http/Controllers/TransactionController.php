<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Item;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class TransactionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $data['date'] = Carbon::today();
        $data['transactions'] = Transaction::select('*')
                                ->whereDate('created_at', Carbon::today())
                                ->where(['user_id' => $user->id])
                                ->orderBy('updated_at', 'desc')
                                ->paginate(10);
        $data['items'] = Item::select('*')
                            ->where('quantity', '>', 0)
                            ->orderBy('id')
                            ->get();
        
        // Pending = 0, Approved = 1, Rejected = 2
        $data['status'] = [
            'Pending',
            'Approved',
            'Rejected'
        ];
        if (!$user->isAdmin) {
            return view('transactions/index', $data);
        } else {
            return Redirect::to('approval')
            ->with('error', 'You are not allowed to see this page');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Request stock
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function request(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'id' => 'required',

        ]);

        $item = Item::where(['id' => $request->id])->first();
        Transaction::create([
            'stock_no'           => $item->stock_no,
            'unit'               => $item->unit,
            'description'        => $item->description,
            'quantity_requested' => $request->quantity_requested,
            'quantity_issued'    => 0,
            'approved'           => 0,
            'item_id'            => $request->id,
            'added_on'           => $request->added_on,
            'user_id'            => $user->id,
        ]);
        return Redirect::to('/transactions')
        ->with('success', 'Request successfully submitted');
    }


    /**
     * Display a listing of the requests that need approval.
     *
     * @return \Illuminate\Http\Response
     */
    public function approval()
    {
        $user = Auth::user();
        $data['transactions'] = Transaction::select(['iar_no', 'transactions.id', 'ris_no', 'stock_no', 'unit', 'description', 'quantity_issued', 'quantity_requested', 'first_name', 'last_name', 'approved'])
                                    ->join('users', 'transactions.user_id', '=', 'users.id')
                                    ->orderBy('transactions.created_at', 'desc')
                                    ->paginate(10);
        $data['items'] = Item::select('*')
                            ->orderBy('id')
                            ->get();
        
        // Pending = 0, Approved = 1, Rejected = 2
        $data['status'] = [
            'Pending',
            'Approved',
            'Rejected'
        ];
        if ($user->isAdmin) {
            return view('transactions/approval', $data);
        } else {
            return Redirect::to('transactions')
            ->with('error', 'You are not allowed to see this page');
        }
        
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['transaction'] = Transaction::where(['id' => $id])->first();

        return view('transactions/edit', $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'quantity_issued' => 'required'
        ]);

        $update_transaction = [
            'quantity_issued' => $request->quantity_issued,
            'approved'        => 1,
            'ris_no'          => $request->ris_no
        ];
        $item_id = Transaction::where(['id' => $id])->first()->item_id;
        Transaction::where(['id' => $id])->update($update_transaction);

        $item_details = Item::where(['id' => $item_id])
            ->decrement('quantity', $request->quantity_issued);

        return Redirect::to('approval')
                        ->with('success', 'Approved Request');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->isAdmin) {
            Transaction::where(['id' => $id])->update(['approved' => 2]);
            return Redirect::to('approval')->with('success', 'Succesfully deleted transaction');
        } else {
            Transaction::where(['id' => $id])->delete();
            return Redirect::to('transactions')->with('success', 'Succesfully deleted transaction');
        }
        
    }

    /**
     * Display the report for today
     *
     * @return \Illuminate\Http\Response
     */
    public function report(Request $request)
    {
        $user = Auth::user();
        $where_date = Carbon::today();
        if (isset($request->date)) {
            $where_date = new Carbon($request->date);
        }
        
        $data['transactions'] = Transaction::select('*')
                                    ->whereDate('added_on', $where_date)
                                    ->where(['approved' => 1])
                                    ->where('ris_no', '<>', '')
                                    ->orderBy('added_on', 'desc')
                                    ->get();
        $data['items'] = Item::select('*')
                            ->orderBy('id')
                            ->get();
        // Pending = 0, Approved = 1, Rejected = 2
        $data['status'] = [
            'Pending',
            'Approved',
            'Rejected'
        ];
        $data['date'] = $where_date;
        if ($user->isAdmin) {
            return view('transactions/report', $data);
        } else {
            return Redirect::to('transactions')
            ->with('error', 'You are not allowed to see this page');
        }
        
    }

    /**
     * Display the request report for today
     *
     * @return \Illuminate\Http\Response
     */
    public function reportRequest()
    {
        $user = Auth::user();
        $data['transactions'] = Transaction::select('*')
                                    ->whereDate('added_on', Carbon::today())
                                    ->where(['approved' => 1])
                                    ->orderBy('added_on', 'desc')
                                    ->get();
        $data['items'] = Item::select('*')
                            ->orderBy('id')
                            ->get();
        // Pending = 0, Approved = 1, Rejected = 2
        $data['status'] = [
            'Pending',
            'Approved',
            'Rejected'
        ];
        if ($user->isAdmin) {
            return view('transactions/request', $data);
        } else {
            return Redirect::to('transactions')
            ->with('error', 'You are not allowed to see this page');
        }
        
    }

    /**
     * Display the request report for today
     *
     * @return \Illuminate\Http\Response
     */
    public function reportRis(Request $request)
    {
        $user       = Auth::user();
        $where_date = Carbon::today();

        if (isset($request->date)) {
            $where_date = new Carbon($request->date);
        }
        
        $data['transactions'] = Transaction::select('*')
                                    ->whereDate('added_on', $where_date)
                                    ->where(['user_id' => $user->id])
                                    ->where(['approved' => 1])
                                    ->orderBy('added_on', 'desc')
                                    ->get();
        $data['items'] = Item::select('*')
                            ->orderBy('id')
                            ->get();
        // Pending = 0, Approved = 1, Rejected = 2
        $data['status'] = [
            'Pending',
            'Approved',
            'Rejected'
        ];

        $data['date'] = $where_date;
        return view('transactions/ris', $data);
        
    }
}
