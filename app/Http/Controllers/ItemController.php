<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Transaction;
use Redirect;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ItemController extends Controller
{

    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = Auth::user();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        if ($user->isAdmin) {
            $description = isset($request->description) ? "%" . $request->description . "%": "";
            $data['description'] = $request->description;
            if ($description) {
                $data['items'] = Item::select('*')
                                ->where('description', 'LIKE', $description)
                                ->orderBy('id', 'desc')
                                ->paginate(10);
            } else {
                $data['items'] = Item::select('*')
                                ->orderBy('id', 'desc')
                                ->paginate(10);
            }
            
            
            return view('items/index', $data);       
        } else {
            return Redirect::to('transactions')
            ->with('error', 'You are not allowed to see this page');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->isAdmin) {
            return view('items/add');
        } else {
            return Redirect::to('transactions')
            ->with('error', 'You are not allowed to see this page');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'stock_no'    => 'required',
            'unit'        => 'required',
            'description' => 'required',
            'quantity'    => 'required'
        ]);

        Item::create($request->all());
    
        return Redirect::to('items')
       ->with('success','Item added succesfully!');
    }

    /**
     * Show the form for updating stock
     *
     * @return \Illuminate\Http\Response
     */
    public function add($id)
    {
        $user = Auth::user();
        if ($user->isAdmin) {
            $data['date'] = Carbon::today();
            $data['item'] = Item::where(['id' => $id])->first();
            $data['id'] = $id;
            return view('items/update', $data);
        } else {
            return Redirect::to('items')
            ->with('error', 'You are not allowed to see this page');
        }
        
    }

    /**
     * Update stock
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateStock(Request $request, $id)
    {
        $user = Auth::user();
        $request->validate([
            'iar_no'   => 'required',
            'added_on' => 'required',
            'quantity' => 'required',
        ]);
        $item = Item::where(['id' => $id])->first();
        $newQuantity = $request->quantity + $item->quantity;
        $update = [
            'quantity'    => $newQuantity,
        ];
        Item::where('id', $id)->update($update);

        Transaction::create([
            'stock_no'           => $item->stock_no,
            'unit'               => $item->unit,
            'description'        => $item->description,
            'quantity_requested' => 0,
            'quantity_issued'    => $request->quantity,
            'approved'           => 1,
            'iar_no'             => $request->iar_no,
            'item_id'            => $request->id,
            'added_on'           => $request->added_on,
            'user_id'            => $user->id,
        ]);
        return Redirect::to('items')->with('success','Item was successfully updated');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $where_date = Carbon::today();
        if ($user->isAdmin) {
            $where = ['id' => $id];
            $data['item'] = Item::where($where)->first();
            $data['transaction'] = Transaction::where(['item_id' => $id, 'approved' => 1])
                                                ->where('ris_no', '<>', '')
                                                ->sum('quantity_issued');
            $data['records'] = Transaction::select('*')
                                ->where(['item_id' => $id, 'approved' => 1])
                                ->orderBy('added_on', 'asc')
                                ->get();
            // Pending = 0, Approved = 1, Rejected = 2
            $data['status'] = [
                'Pending',
                'Approved',
                'Rejected'
            ];
            $data['date'] = $where_date;
            return view('items/stock', $data);
        } else {
            return Redirect::to('items')
            ->with('error', 'You are not allowed to see this page');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if ($user->isAdmin) {
            $where = ['id' => $id];
            $data['item'] = Item::where($where)->first();
            return view('items/edit', $data);
        } else {
            return Redirect::to('items')
            ->with('error', 'You are not allowed to see this page');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'stock_no'    => 'required',
            'unit'        => 'required',
            'description' => 'required',
            'quantity'    => 'required'
        ]);
         
        $update = [
            'stock_no'    => $request->stock_no,
            'unit'        => $request->unit,
            'description' => $request->description,
            'quantity'    => $request->quantity,
        ];
        Item::where('id', $id)->update($update);
   
        return Redirect::to('items')
       ->with('success','Item was successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Item::where(['id' => $id])->delete();
        return Redirect::to('items')
       ->with('delete','Item was successfully deleted');
    }
}
