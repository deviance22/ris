<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/transactions/request', 'TransactionController@request')->name('transactions.request');
Route::resource('transactions', 'TransactionController'); 

Route::get('/approval', 'TransactionController@approval')->name('transactions.approval');
Route::resource('transactions', 'TransactionController');

Route::get('/report', 'TransactionController@report')->name('transactions.report');
Route::resource('transactions', 'TransactionController');

Route::get('/ris', 'TransactionController@reportRis')->name('transactions.report_ris');
Route::resource('transactions', 'TransactionController');

Route::get('/request', 'TransactionController@reportRequest')->name('transactions.report_request');
Route::resource('transactions', 'TransactionController');

Route::get('/add/{id}', 'ItemController@add')->name('items.add');
Route::resource('items', 'ItemController');

Route::post('/updateStock/{id}', 'ItemController@updateStock')->name('items.updateStock');
Route::resource('items', 'ItemController');

Route::resource('items', 'ItemController'); 
Route::resource('transactions', 'TransactionController');
